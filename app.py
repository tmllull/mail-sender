from dotenv import dotenv_values
import smtplib
import argparse
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.utils import formatdate
from email import encoders
import os

# Check if env variables are setted
try:
    SERVER_HOST = os.environ["SERVER_HOST"]
    SERVER_PORT = os.environ["SERVER_PORT"]
    SERVER_USER = os.environ["SERVER_USER"]
    SERVER_PASS = os.environ["SERVER_PASS"]
except:
    #Load .env
    config = dotenv_values(".env")
    SERVER_HOST = config["SERVER_HOST"]
    SERVER_PORT = config["SERVER_PORT"]
    SERVER_USER = config["SERVER_USER"]
    SERVER_PASS = config["SERVER_PASS"]

parser = argparse.ArgumentParser()

parser.add_argument("-from", dest="send_from",
                    default=SERVER_USER, help="Email from")
parser.add_argument("-to", dest="send_to",
                    default=None, required=True, help="Destination email. You can add more than one email separated with commas (without spaces)")
parser.add_argument("-subject", dest="subject",
                    default=None, required=True, help="The subject you want to set")
parser.add_argument("-message", dest="message",
                    default=None, help="The message (string) you want to send")
parser.add_argument("-message-from-file", dest="message_from_file",
                    default=None, help="Path to file from where read the message content")
parser.add_argument("-tls", dest="tls",
                    default="True", help="Set if you want to use TLS or no. (yes/no")
parser.add_argument("-format", dest="format",
                    default="plain", help="Send email with specific format: text or html")
parser.add_argument("-files", dest="files",
                    default="", help="Path to files. Comma separated")

args = parser.parse_args()

if args.message is not None and args.message_from_file is not None:
    print("Only 1 type of message are allowed")
    parser.print_usage()
    exit()
if args.message is None and args.message_from_file is None:
    print("You must set at least '-message' or '-message-from-file'")
    parser.print_usage()
    exit()

FROM = args.send_from
TO = args.send_to.split(",")
SUBJECT = args.subject
# MESSAGE = args.message
if args.tls.lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup']:
    TLS = True
elif args.tls.lower() in ['false', '0', 'f', 'n', 'no', 'nope', 'nah']:
    TLS = False
else:
    exit("Unrecognized tls param")
if args.format.lower() in ['text', 'plain', 't']:
    FORMAT = "plain"
elif args.format.lower() in ['html', 'web', 'h']:
    FORMAT = "html"
else:
    exit("Unrecognized format param")
FILES = args.files.split(",")

msg = MIMEMultipart()
msg['From'] = FROM
msg['To'] = ", ".join(TO)
msg['Date'] = formatdate(localtime=True)
msg['Subject'] = SUBJECT

if args.message is not None:
    MESSAGE = args.message

elif args.message_from_file is not None:
    MSG = open(args.message_from_file)
    MESSAGE = MSG.read()
else:
    exit("There are an error with the message")

msg.attach(MIMEText(MESSAGE))
if FILES[0] != "":
    for path in FILES:
        part = MIMEBase('application', "octet-stream")
        with open(path, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(Path(path).name))
        msg.attach(part)

smtp = smtplib.SMTP(SERVER_HOST, SERVER_PORT)

if TLS:
    smtp.starttls()
try:
    smtp.login(SERVER_USER, SERVER_PASS)
    smtp.sendmail(FROM, TO, msg.as_string())
    smtp.quit()
except Exception as e:
    print("There was an error sending the email:"+str(e))

print("Email sended succesfully!")
