Send an email using python and docker

1. Create .env file:

```
SERVER_HOST=server_host
SERVER_PORT=server_port
SERVER_USER=server_user
SERVER_PASS=server_pass
```

2. Run:

```shell
docker build --tag mail-sender .
```

3. Run:

```shell
docker run -it --env-file /path/to/env/file/.env --rm mail-sender app.py [ARGS]
```

If you want to run with a cron job, use:

```shell
docker run --env-file /path/to/env/file/.env --rm mail-sender app.py [ARGS]
```

ARGS:

```shell
-h, --help          show this help message and exit
-from               Email from. Default: SERVER_USER. Optional
-to                 Destination email. You can add more than one email separated with commas (without spaces). Required
-subject            The subject you want to set. Required
-message            The message (string) you want to send. Required if not use '-message-from-file' arg
-message-from-file  Path to file from where read the message content. Required if not use '-message' arg
-tls                Set if you want to use TLS (y) or no (n). Default: y. Optional
-format             Send email with specific format: text or html. Default: text. Optional
-files              Path to files. Comma separated. Optional
```

4. If you want to run the script without Docker:

```shell
pip install -r requirements.txt
python app.py [ARGS]
```
